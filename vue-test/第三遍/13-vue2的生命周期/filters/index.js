import addCount$ from '@/filters/addCount$'
export default (Vue)=>{
	Vue.use(addCount$)
}