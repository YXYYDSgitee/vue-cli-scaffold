import bgC from "@/directives/bgC";
import violet from "@/directives/violet";

export default (Vue)=>{
	Vue.use(bgC);
	Vue.use(violet);
}