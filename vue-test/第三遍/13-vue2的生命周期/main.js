import Vue from 'vue'
import App from './App.vue'
import directives from "@/directives";
import filters from "@/filters"
Vue.config.productionTip = false

Vue.use(directives)
Vue.use(filters)

new Vue({
  render: h => h(App),
}).$mount('#app')
