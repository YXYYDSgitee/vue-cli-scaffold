import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false

// 注册全局过滤器
Vue.filter('addCount$',(value)=>{
  return '$'+value
})
new Vue({
  render: h => h(App),
}).$mount('#app')
