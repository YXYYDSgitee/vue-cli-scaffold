import Vue from 'vue'
import App from './App.vue'
import myPlugin from "@/plugin/my";
Vue.config.productionTip = false

//Vue 提供一个use方法 专门用来 定义 插件
Vue.use(myPlugin)

new Vue({
  render: h => h(App),
}).$mount('#app')
