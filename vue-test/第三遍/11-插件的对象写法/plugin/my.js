import Button from "@/components/Button";
export default {
	install(Vue){
		//插件的作用
		// 1- 添加全局方法或者属性
		Vue.prototype.$bus = 'bus';
		// 2- 添加全局指令
		Vue.directive('violet',(el)=>{
			el.style.background = '#8e24aa'
		});
		Vue.directive('bgC',(el)=>{
			el.style.background = '#80cbc4'
		});
		// 3- 添加全局过滤器
		Vue.filter('addCount$',(value)=>{
			return "$" + value
		})
		// 4-添加一个全局注册的组件
		Vue.component("Button", Button);
	}
}