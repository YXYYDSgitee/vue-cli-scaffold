import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/pages/Home";
import Login from "@/pages/Login";
import NotFound from "@/pages/NotFound";
import News from "@/pages/Home/News";
import Music from "@/pages/Home/Music";
import Game from "@/pages/Home/Game";

// 应用路由插件（比如注册一些router提供的全局组件等等...）
Vue.use(VueRouter)

// 创建路由对象，并且内部配置模式和路由  并导出
export const router = new VueRouter({
	mode:'history',
	routes:[
		{
			// 有时候，通过一个名称来标识一个路由显得更方便一些，特别是在链接一个路由，或者是执行一些跳转的时候。
			// 你可以在创建 Router 实例的时候，在 routes 路由表配置中给某个路由设置名称name
			path: '/Home',
			/* 每一个对象都是一个路由规则,我们可以给路由规则一个名字,方便后边使用 */
			// name:'Home',
			/* 默认子路由写法3:重定向推荐写法 */
			redirect:'/Home/News',
			component: Home,
			children:[
				{
					path:'News',
					name:'News',
					component:News
				},
				{
					path:'Music',
					name:'Music',
					component:Music
				},
				{
					path:'Game',
					name:'Game',
					component:Game
				},
				/* 默认子路由写法2:直接定向设置组件写法 */
				// {
				// 	path:'',
				// 	component:News
				// },
				// 默认子路由写法1:重定向推荐写法
				// {
				// 	path:'',
				// 	redirect:'News'
				// }
			]
		},
		{
			path:"/",
			component:Home
		}
		,
		{
			path:'/Login',
			name:'Login',
			component:Login
		},
		//404配置卸载最下边
		{
			path:'/*',
			name:'NotFound',
			component:NotFound
		}
	]
})