import axios from "axios";

const state = {
	movieList: []
}
const getters = {
	movieListNum(state) {
		return state.movieList.length
	}
}
const mutations = {
	addMovieList(state, payload) {
		state.movieList = payload.movieList
	}
}
const actions = {
	async getMovieList({commit}) {
		const result = await axios.get("https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a")
		commit("addMovieList", {movieList: result.data.data.list});
	}
}
export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}