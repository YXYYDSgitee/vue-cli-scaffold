export default {
	namespaced: true,
	//数据,state的值必须是一个对象!!!! 对象中保存我们想要的数据
	state: {
		num: 1
	},
	//getters:vuex中的计算属性,可以根据当前store的数据或者计算属性,再次计算一个值
	getters: {
		doubleNum(state) {
			return state.num * 2
		}
	},
	//mutations:修改state数据的唯一来源
	//mutations的第一个参数是当前store的state对象，修改state的时候要遵守响应式数据改动规则(vuex响应式原理其实是在vuex内部也new Vue,数据放在vm上)
	mutations: {
		//每次加1
		increment(state) {
			state.num++;
		},
		
		//每次加n
		incrementN(state, {n}) {
			state.num += n;
		},
	},
	actions: {
		incrementAsync({commit}, payload) {
			setTimeout(() => {
				commit('increment')
			}, 1000)
		},
		incrementNAsync({commit}, payload) {
			setTimeout(() => {
				commit('incrementN', payload)
			}, 1000)
		}
	}
}