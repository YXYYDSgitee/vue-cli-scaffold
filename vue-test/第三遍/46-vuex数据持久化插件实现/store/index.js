import Vue from "vue";
import Vuex from "vuex";
import Count from "@/store/modules/Count";
import Movie from "@/store/modules/Movie";
import createPersistedState from 'vuex-persistedstate'

//应用Vuex插件
Vue.use(Vuex);

// 实例化Vuex仓库
const store = new Vuex.Store({
	modules: {
		Count: Count,
		Movie: Movie
	},
	plugins: [createPersistedState()]
})

// 暴露
export default store
