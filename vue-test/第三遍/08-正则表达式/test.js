// const reg = /^[0-9a-zA-z]{3,6}$/g;   // 匹配 0-9a-zA-z  三到六个
// const s1 = '123456';
// const s2 = 'a12344'
// const s3 = '123'

// test 方法不能连着使用  不然 会不精准
// console.log(reg.test(s1));
// console.log(reg.test(s2));
// console.log(reg.test(s3));

// const reg = /[0-9]+/g;  // 配置 0-9 的数字 + 代表一次或者多次
// const str = '12da31dasd2da98hjshda';
// console.log(str.match(reg));  // 把匹配正则的数字转正 数组

/**
 * 常见面试题:
 *     把一组数字,使用千位分隔符分割
 *     "1234567890"--->"1,234,567,890"
 *     \d{3}:匹配固定3个连续数字
 *     (\d{3})+$:匹配一组或多组 3个连续的数组,并且是以这个为结尾
 *     正则1 ?= 正则2:?=是前瞻,当前规则的含义是:匹配符和 正则2的字符 前边紧贴的字符, 并且符和正则1的
 * */

const reg = /(\d)(?=(\d{3})+$)/g;
const str = '1234567890';

// 把匹配成功的数字  替换成  匹配成功的数字,
console.log(str.replace(reg, "$1,"));  // $1,  是当前数字加逗号
