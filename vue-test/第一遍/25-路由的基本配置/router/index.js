import Vue from "vue";
import VueRouter from "vue-router"
import Home from "@/pages/Home";
import Login from "@/pages/Login";

// 使用路由插件
Vue.use(VueRouter);

export const router = new VueRouter({
	mode:'history',
	routes:[
		{
			path:'/Login',
			component:Login
		},
		{
			path:'/Home',
			component:Home
		}
	]
})