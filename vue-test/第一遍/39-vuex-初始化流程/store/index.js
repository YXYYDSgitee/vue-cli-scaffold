import Vue from "vue";
import Vuex from 'vuex'

// 应用插件
Vue.use(Vuex);

// 实例化Vuex的仓库
const store = new Vuex.Store({
	//数据,state的值必须是一个对象!!!! 对象中保存我们想要的数据
	state: {
		num: 0,
	},
})

// 暴露仓库
export default store