import Vue from 'vue'
import App from './App.vue'
import myDirectives from './directives'
import myFilters from './filters'
Vue.config.productionTip = false

//Vue提供了一个use方法,可以使用插件
Vue.use(myDirectives);
Vue.use(myFilters);

new Vue({
  render: h => h(App),
}).$mount('#app')
