import red from "@/directives/red";
import bgColor from "@/directives/bgColor";

export default (Vue)=>{
	Vue.use(red)
	Vue.use(bgColor)
}