export default (Vue)=>{
	Vue.directive("bgColor", (el, binding) => {
		el.style.background = binding.value;
	});
}