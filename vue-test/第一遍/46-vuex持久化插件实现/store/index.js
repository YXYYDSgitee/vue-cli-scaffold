import Vue from "vue";
import Vuex from "vuex";
import Count from './modules/Count'
import Movie from './modules/Movie'
// vuex 持久化插件
import createPersistedState from 'vuex-persistedstate'
//应用Vuex插件
Vue.use(Vuex);

// 实例化Vuex仓库
const store = new Vuex.Store({
	modules: {
		Count: Count,
		Movie: Movie
	},
	plugins: [createPersistedState()]  // 使用插件  直接通过本地存储保存了 整个vuex
})

// 暴露
export default store
