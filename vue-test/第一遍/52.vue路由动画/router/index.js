import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

const One = () => import("@/pages/One");
const Two = () => import("@/pages/Two");
export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/one",
      component: One,
    },
    {
      path: "/two",
      component: Two,
    },
  ],
});
