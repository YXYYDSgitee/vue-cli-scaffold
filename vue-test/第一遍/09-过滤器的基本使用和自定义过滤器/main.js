import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 注册全局过滤器
Vue.filter('priceFormat',(value,arg=2)=>{
  return (+value).toFixed(arg)
})

Vue.filter("add$", (value) => {
  return "￥" + value;
});

new Vue({
  render: h => h(App),
}).$mount('#app')
