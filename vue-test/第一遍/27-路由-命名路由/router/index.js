import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/pages/Home";
import Login from "@/pages/Login";

// 应用路由插件（比如注册一些router提供的全局组件等等...）
Vue.use(VueRouter)

// 创建路由对象，并且内部配置模式和路由  并导出
export const router = new VueRouter({
	mode:'history',
	routes:[
		{
			// 有时候，通过一个名称来标识一个路由显得更方便一些，特别是在链接一个路由，或者是执行一些跳转的时候。
			// 你可以在创建 Router 实例的时候，在 routes 路由表配置中给某个路由设置名称name
			path:'/Home',
			name:'Home',
			component:Home
		},
		{
			path:'/Login',
			name:'Login',
			component:Login
		}
	]
})