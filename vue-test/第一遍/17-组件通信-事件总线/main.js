import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false

//中介要求:1. 放在所有组件都能访问的位置 2.必须是一个实例,允许绑定和调用自定义事件
// Vue.prototype.$bus = new Vue();

new Vue({
  render: h => h(App),
  beforeCreate() {
    // 第二种写法
    Vue.prototype.$bus = this;
  }
}).$mount('#app')
