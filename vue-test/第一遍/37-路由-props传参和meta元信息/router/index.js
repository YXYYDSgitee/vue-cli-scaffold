import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "@/pages/Home";
// import Login from "@/pages/Login";
// import NotFound from "@/pages/NotFound";
// import News from "@/pages/Home/News";
// import Music from "@/pages/Home/Music";
// import Game from "@/pages/Home/Game";


/*
  路由懒加载:
    结合 Vue 的异步组件和 Webpack 的代码分割功能，轻松实现路由组件的懒加载
    - 不再使用import模板化语法引入路由组件
    - 而是定义一个当前组件对应的函数,函数内部异步使用import方法引入组件,并返回promise实例即可
    - 当路由被切换需要加载当前路由的时候,就会调用当前函数
*/
const Home = () => import("@/pages/Home");
const Login = () => import("@/pages/Login");
const News = () => import("@/pages/Home/News");
const Music = () => import("@/pages/Home/Music");
const Game = () => import("@/pages/Home/Game");
const NotFound = () => import("@/pages/NotFound");
const MusicDetail = () => import("@/pages/Home/Music/MusicDetail")
const NewsDetail = () => import('@/pages/Home/News/NewsDetail')
const GameDetail = () => import('@/pages/Home/Game/GameDetail')

// 应用路由插件（比如注册一些router提供的全局组件等等...）
Vue.use(VueRouter)

// 创建路由对象，并且内部配置模式和路由  并导出
export const router = new VueRouter({
	mode:'history',
	routes:[
		{
			// 有时候，通过一个名称来标识一个路由显得更方便一些，特别是在链接一个路由，或者是执行一些跳转的时候。
			// 你可以在创建 Router 实例的时候，在 routes 路由表配置中给某个路由设置名称name
			path: '/Home',
			/* 每一个对象都是一个路由规则,我们可以给路由规则一个名字,方便后边使用 */
			// name:'Home',
			/* 默认子路由写法3:重定向推荐写法 */
			redirect:'/Home/News',
			component: Home,
			children:[
				{
					path:'News',
					name:'News',
					component:News,
					children:[{
						path: 'NewsDetail',
						name:'NewsDetail',
						component:NewsDetail,
						//如果props是一个函数,则可以在内部接受到当前路由组件的$route对象
						props:(route)=>{
							console.log(route, "route");
							return {
								...route.params,
								...route.query,
								classRoom: "0516",
								count: "71",
							};
						}
					}]
				},
				{
					path:'Music',
					name:'Music',
					component:Music,
					children:[
						{
							path:'MusicDetail/:id/:age?',
							name:'MusicDetail',
							component:MusicDetail,
							props: true,
						}
					]
				},
				{
					path:'Game',
					name:'Game',
					component:Game,
					children:[
						{
							path: 'GameDetail/:id',
							name:'GameDetail',
							component:GameDetail,
							meta:{
								hidden: true,
								icon: "plus",
							},
							props: (route)=>{
								return {
									...route.params,
									...route.query,
									...route.meta
								}
							}
						}
					]
				},
				/* 默认子路由写法2:直接定向设置组件写法 */
				// {
				// 	path:'',
				// 	component:News
				// },
				// 默认子路由写法1:重定向推荐写法
				// {
				// 	path:'',
				// 	redirect:'News'
				// }
			]
		},
		{
			path:"/",
			component:Home
		}
		,
		{
			path:'/Login',
			name:'Login',
			component:Login
		},
		//404配置卸载最下边
		{
			path:'/*',
			name:'NotFound',
			component:NotFound
		}
	]
})