import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
const role = "admin";
// 注册全局自定义指令
Vue.directive('role',{
  //只调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置(在这个函数中无法获取到父元素)
  bind(el,binding){
    console.log('bind')
  },
  //被绑定元素插入父节点时调用(可以获取到父元素)
  inserted(el,binding){
    if (binding.value != role){
      el.remove()
    }
    console.log('inserted')
  },
  //所在组件的 VNode 更新时调用,但是可能发生在其子 VNode 更新之前
  update(el,binding){
    console.log('update')
  },
  //componentUpdated：指令所在组件的 VNode 及其子 VNode 全部更新后调用。
  componentUpdated(el,binding){
    console.log('componentUpdated')
  },
  // unbind：只调用一次，指令与元素解绑时调用
  unbind(el,binding){
    console.log('unbind')
  }
})

new Vue({
  render: h => h(App),
}).$mount('#app')
