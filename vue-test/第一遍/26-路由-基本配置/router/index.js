import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/pages/Home";
import Login from "@/pages/Login";

// 应用路由插件（比如注册一些router提供的全局组件等等...）
Vue.use(VueRouter)

// 创建路由对象，并且内部配置模式和路由  并导出
export const router = new VueRouter({
	mode:'history',
	routes:[
		{
			path:'/Home',
			component:Home
		},
		{
			path:'/Login',
			component:Login
		}
	]
})