/* 
  混入对象中拥有完整的 Vue配置的选项
*/
export default {
  data() {
    return {
      count: 0,
    };
  },
  methods: {
    changeCount() {
      this.count++;
    },
  },
  mounted() {
    console.log("mixins");
  },
};
