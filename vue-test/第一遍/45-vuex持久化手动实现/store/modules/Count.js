//Vuex 允许我们将 store 分割成模块（module）。
//每个模块拥有自己的 state、mutation、action、getter
//每个模块其实是一个对象,暴露出去将来放在new Vuex.Store的配置项中
export default {
	//添加命名空间,方便我们使用辅助函数获取
	namespaced: true,
	
	//数据,state的值必须是一个对象!!!! 对象中保存我们想要的数据
	state: JSON.parse(localStorage.getItem("vuex_count")) || {
		num: 0,
	},
	//getters:vuex中的计算属性,可以根据当前store的数据或者计算属性,再次计算一个值
	getters: {
		doubleNum(state) {
			return state.num * 2
		}
	},
	//mutations:修改state数据的唯一来源
	//mutations的第一个参数是当前store的state对象，修改state的时候要遵守响应式数据改动规则(vuex响应式原理其实是在vuex内部也new Vue,数据放在vm上)
	mutations: {
		//每次加1
		increment(state) {
			state.num++;
			localStorage.setItem("vuex_count", JSON.stringify(state));
		},
		
		//每次加n
		incrementN(state, {n}) {
			state.num += n;
			localStorage.setItem("vuex_count", JSON.stringify(state));
		},
	},
	// 书写异步函数逻辑的
	actions: {
		incrementWait({commit}) {
			/*
			  actions中方法的参数是一个阉割版的store,我们可以直接解构出commit使用
			*/
			setTimeout(() => {
				commit("increment");
			}, 1000);
		},
		
		incrementNWait({commit}, payload) {
			setTimeout(() => {
				commit("incrementN", payload);
			}, 1000);
		},
	},
}