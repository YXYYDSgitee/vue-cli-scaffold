import Vue from 'vue'
import App from './App.vue'
import plugin from "@/plugin";
Vue.config.productionTip = false

//Vue提供了一个use方法,可以使用插件
Vue.use(plugin);

new Vue({
  render: h => h(App),
}).$mount('#app')
