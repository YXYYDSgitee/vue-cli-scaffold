import Button from "@/components/Button";
//vue的插件形式1:是一个对象,必须提供 install 方法,当插件被安装的时候,install方法会自动的调用
//插件安装install方法被调用的时候,会传递进来一个
export default {
	install(Vue) {
		//插件的作用
		//1.添加全局方法或者属性
		Vue.prototype.$bus = "bus";
		//2.添加全局指令
		Vue.directive("red", (el, binding) => {
			el.style.background = "red";
		});
		//3.添加全局过滤器
		Vue.filter("addCount$", (value) => {
			return "$" + value;
		});
		//4.添加一个全局注册的组件
		Vue.component("Button", Button);
		//5.....
	},
};