// 数组方法复习 --- reduce


// 统计一个字符串中出现次数最多的字符,以及打印他的次数
// const str='ubnjsafgjiasifjfaskfjoasjrfgruwagrjqw';
// const arr = str.split('');
// const count = arr.reduce((pre,cur)=>{
// 	if (pre[cur]){
// 		pre[cur] += 1
// 	}else{
// 		pre[cur] = 1
// 	}
// 	return pre
// },{})
// let maxNum = 0
// let maxStr = ''
// for (const countKey in count) {
// 	if (count[countKey] > maxNum){
// 		maxNum = count[countKey];
// 		maxStr = countKey;
// 	}
// }
// console.log('maxNum',maxNum,'maxStr',maxStr); // maxNum 6 maxStr j

// 已知一个地址"http://www.laoli.com?a=1&b=2&c=3",把查询字符串转为对象{a:1,b:2,c:3}
// const urlStr = "http://www.laoli.com?a=1&b=2&c=3";
// const arr =urlStr.split('?');  // ['http://www.laoli.com','a=1&b=2&c=3']
// const arr2 = arr[1].split('&'); // ['a=1','b=2','c=3']
// const obj = arr2.reduce((pre,cur)=>{
// 	// console.log(cur.split('=')); // [ 'a', '1' ] [ 'b', '2' ] [ 'c', '3' ]
// 	pre[cur.split('=')[0]] = +cur.split('=')[1]
// 	return pre
// },{})
// console.log(obj); // { a: 1, b: 2, c: 3 }
