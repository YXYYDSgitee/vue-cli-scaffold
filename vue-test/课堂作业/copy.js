// 浅拷贝
function shallowCopy(value){
	// console.log(typeof value)
	if (typeof value === 'object') {
		let result = value instanceof Array ? [...value]:{...value};
		return result
	}else {
		const result = value;
		return result
	}
}
// let obj = {name:'a',age:'20'}
// const test = shallowCopy(obj)
// obj.name='b'
// console.log(test)

// 深拷贝
function deepCopy(value){
	let result = null
	if (value instanceof Array){
		result = []
	}else if (value instanceof Object){
		result = {}
	}else{
		return value
	}
	for (let item in value) {
		// console.log(value[item] instanceof Array)
		result[item] = deepCopy(value[item])
	}
	return result
}
let arr =[1,3,[1,2,3,5]]
const test2 = deepCopy(arr)
arr[0] = 2;
arr[2][0] = 77
// const obj = {name:'a',age:{hobby:'钓鱼'}}
// const test2 = deepCopy(obj)
// obj.name='b';
// obj.age.hobby='鲨鱼'
console.log(test2)
