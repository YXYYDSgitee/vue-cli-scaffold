import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false

// 事件总线第一种写法：中介C
// Vue.prototype.$bus=new Vue();

new Vue({
  render: h => h(App),
  beforeCreate(){
    // 事件总线第二种写法：中介C
    Vue.prototype.$bus=this
  }
}).$mount('#app')
