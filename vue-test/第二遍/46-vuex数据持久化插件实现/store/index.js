import Vue from "vue";
import Vuex from "vuex";
import Count from "./modules/Count";
import Movie from "./modules/Movie";
import createPersistedState from 'vuex-persistedstate'

//应用Vuex插件
Vue.use(Vuex);

// 实例化Vuex仓库
const store = new Vuex.Store({
	modules: {
		Count: Count,
		Movie: Movie
	},
	plugins: [createPersistedState()]  // 应用插件  数据持久化
})

// 暴露
export default store
