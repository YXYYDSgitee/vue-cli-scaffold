import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 全局自定义 过滤器
Vue.filter('addCount$',(value)=>{
  return "$"+ value
})
new Vue({
  render: h => h(App),
}).$mount('#app')
