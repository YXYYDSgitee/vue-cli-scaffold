const reg = /^[0-9a-zA-Z]{3,6}$/g;
let s1 = '123456';
let s2 = 'a23456';
let s3 = '1256';
console.log(reg.test(s1));
console.log(reg.test(s2));
console.log(reg.test(s3));

/* const reg = /日本|钓鱼岛|政府/g;
let str = "钓鱼岛是中国的，不是日本这个政府的，日本的政府新闻公告。。。。。";
const re = str.replace(reg, "**");
console.log(re); */

/* const reg = /[0-9]+/g;
let str = "1hsa2ds345dfdf44df";
console.log(str.match(reg)); */

/*
  常见面试题:
    把一组数字,使用千位分隔符分割
    "1234567890"--->"1,234,567,890"
    \d{3}:匹配固定3个连续数字
    (\d{3})+$:匹配一组或多组 3个连续的数组,并且是以这个为结尾
    正则1 ?= 正则2:?=是前瞻,当前规则的含义是:匹配符和 正则2的字符 前边紧贴的字符, 并且符和正则1的

*/

const reg = /(\d)(?=(\d{3})+$)/g;
let str = "1234567890";
// const re = str.match(reg);
// console.log(re);[1,4,7]

const re = str.replace(reg, "$1,");
console.log(re);