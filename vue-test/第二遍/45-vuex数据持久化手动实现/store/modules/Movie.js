import axios from "axios";

const state = JSON.parse(localStorage.getItem('vuex-Movie')) || {movieList: []}
const mutations = {
	SET_MOVIELIST_STATE(state, payload) {
		state.movieList = payload.movieList;
		localStorage.setItem('vuex-Movie', JSON.stringify(state))
	},
}
const actions = {
	async getMovieList({commit}) {
		const re = await axios.get(
			"https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a"
		);
		commit("SET_MOVIELIST_STATE", {movieList: re.data.data.list});
	},
}
const getters = {
	movieListNum(state) {
		return state.movieList.length
	}
}
export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}