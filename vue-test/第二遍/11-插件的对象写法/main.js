import Vue from 'vue'
import App from './App.vue'
import plugin from "../../第一遍/11-插件的对象写法/plugin";

Vue.config.productionTip = false

Vue.use(plugin)

new Vue({
  render: h => h(App),
}).$mount('#app')
