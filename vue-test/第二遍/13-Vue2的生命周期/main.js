import Vue from 'vue'
import App from './App.vue'
import directives from '@/directives'
import filters from '@/filters'
Vue.config.productionTip = false

// 全局注册 自定义指令
Vue.use(directives);
// 全局注册 过滤器
Vue.use(filters);

new Vue({
  render: h => h(App),
}).$mount('#app')
