import red from "@/directives/red";
import bgC from "@/directives/bdC"

export default function (Vue){
	Vue.use(red)
	Vue.use(bgC)
}