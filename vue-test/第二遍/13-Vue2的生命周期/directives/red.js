export default (Vue)=>{
	Vue.directive("red", (el, binding) => {
		el.style.background = "red";
	});
}