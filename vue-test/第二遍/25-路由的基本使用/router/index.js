import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/pages/Login";
import Home from "@/pages/Home";

// 使用插件
Vue.use(VueRouter);

export const router = new VueRouter({
	mode:'history',
	routes:[
		{
			path:'/Login',
			component:Login
		},
		{
			path:'/Home',
			component:Home
		}
	]
})