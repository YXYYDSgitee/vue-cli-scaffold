import add$ from "@/filters/add$"

export default function (Vue) {
	Vue.use(add$)
}