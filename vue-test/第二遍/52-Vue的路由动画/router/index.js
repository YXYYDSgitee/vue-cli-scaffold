import Vue from 'vue'
import VueRouter from "vue-router";

Vue.use(VueRouter)

export default new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/Home',
			component: () => import('@/pages/Home')
		},
		{
			path: '/Body',
			component: () => import('@/pages/Body')
		}
	]
})