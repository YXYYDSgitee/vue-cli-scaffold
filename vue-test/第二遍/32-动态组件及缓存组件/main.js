import Vue from "vue";
import App from "./App.vue";
Vue.config.productionTip = false;
new Vue({
  render: (h) => h(App),
  //通过router配置项，把router注入到vue实例中，
  //1-那么整个应用就有了路由功能
  //2-并且每一个实例都会有$router和$route两个对象
}).$mount("#app");
