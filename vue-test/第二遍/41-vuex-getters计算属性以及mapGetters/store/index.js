import Vue from "vue";
import Vuex from "vuex"

// 应用插件
Vue.use(Vuex)

// 实例化Vuex创建store仓库
const store = new Vuex.Store({
	state:{
		num:1
	},
	// vuex 的计算属性
	getters:{
		double(state){
			return state.num*2
		}
	}
})

// 暴露store给入口文件
export default store;